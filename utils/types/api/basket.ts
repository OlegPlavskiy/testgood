export interface Basket {
    response: boolean;
    basket: BasketItem[];
    basketCount: number;
    basketPrice: number;
}

export interface BasketItem {
    id: number;
    name: string;
    price: number;
    count: number;
    poster: string;
    discount: number;
}