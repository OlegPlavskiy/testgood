import { JSONSchemaType } from 'ajv';
import {Basket, BasketItem} from '../../types/api/basket';

const basketItemSchema: JSONSchemaType<BasketItem> = {
    type: "object",
    properties: {
        id: { type: "number" },
        name: { type: "string" },
        price: { type: "number" },
        count: { type: "number" },
        poster: { type: "string" },
        discount: { type: "number" },
    },
    required: ["id", "name", "price", "count", "poster", "discount"],
    additionalProperties: true
};

export const basketSchema: JSONSchemaType<Basket> = {
    type: "object",
    properties: {
        response: { type: "boolean" },
        basket: {
            type: "array"
            //items: basketItemSchema,
        },
        basketCount: { type: "number" },
        basketPrice: { type: "number" },
    },
    required: ["response", "basket", "basketCount", "basketPrice"],
    additionalProperties: true
};


