import Ajv, { JSONSchemaType } from "ajv";
import { Basket } from "../../utils/types/api/basket";
import { basketSchema } from "../../utils/schema/api/basket-schema";

const ajv = new Ajv();

export function validateSchema({ schema, res }: { schema: JSONSchemaType<Basket>, res: any }) {
    const validate = ajv.compile(schema);
    const valid = validate(res);
    if (!valid) {
        console.error(validate.errors);
        throw new Error("Schema validation error");
    }
}