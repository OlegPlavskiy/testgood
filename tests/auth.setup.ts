import { test as setup } from '@playwright/test';
import * as fs from "fs";
import path = require("path");
const { JSDOM } = require('jsdom');

const authFile = '../playwright/.auth/user.json';
const csrfFile = '../playwright/.auth/csrf_token.json';
setup('authenticate', async ({ request }) => {
    await fs.writeFileSync(authFile, '');

    const url = "https://enotes.pointschool.ru/login";

    let responses = await request.get(url);
    const dom = new JSDOM(await responses.text());
    let csrfToken = dom.window.document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    const response = await request.post(url, {
        form: {
            '_csrf': csrfToken,
            'LoginForm[username]': 'test',
            'LoginForm[password]': 'test',
            'LoginForm[rememberMe]': '0',
            'LoginForm[rememberMe]': '1',
            'login-button': ''
        }
    , maxRedirects: 0},);

    const urls = "https://enotes.pointschool.ru";

    let responsess = await request.get(urls);
    const doms = new JSDOM(await responsess.text());
    csrfToken = doms.window.document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    fs.writeFileSync(path.join(__dirname, csrfFile), csrfToken);
    // await request.post('/basket/clear', {data: {}, headers:{
    //         "x-csrf-token": csrfToken}})

    const cookies = [];
    const cookieStrings = response.headers()['set-cookie']
        .replace(/HttpOnly/g, 'HttpOnly=true')
        .split(/[;\n]/);

    cookieStrings.forEach(cookieStr => {
        const parts = cookieStr.split('=');
        const name = parts[0].trim();
        const value = parts[1].trim().split(';')[0]; // Убираем возможные дополнительные параметры
        if(name !== 'HttpOnly')
        cookies.push({
            name: name,
            value: value,
            domain: 'enotes.pointschool.ru',
            path: '/',
            expires: Math.floor(Date.now() / 1000) + 60 * 60 * 24,
            httpOnly: true,
            secure: false,
            sameSite: 'Lax'
        });
    });

    //Сохранение cookies в файл
    fs.writeFileSync(path.join(__dirname, authFile), JSON.stringify({ cookies }), { mode: 0o644 });
});
