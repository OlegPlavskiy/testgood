import { searchTest as test } from '../tests';
import {expect} from "@playwright/test";
import {Basket} from "../../utils/types/api/basket";
import {validateSchema} from "../../utils/schema/validator";
import {basketSchema} from "../../utils/schema/api/basket-schema";

test.describe("Тестовое задание. (Сценарии 2).", () => {
  test.beforeEach(async ({basketApi}) => {
    await basketApi.clear();
    await basketApi.create(1,1);

    const res = await (await basketApi.getBasket()).json();
    expect(res.basketCount).toEqual(1)

  });

  test(`Сценарий 2`, async ({basketApi, productApi }) => {
    const countProduct = 8;
    const resProducts = await productApi.getProducts()
    const resCountProduct = JSON.parse(await (await resProducts.text())).products.length;

    for (let i = 0; i < countProduct; i++) {
      const productId = Math.floor(Math.random() * resCountProduct + 1);
      await basketApi.create(productId,1);
    }

    const res: Basket = await (await basketApi.getBasket()).json();
    expect(res.basketCount).toEqual(9)
    expect(res.basket.length).toEqual(9)

    await validateSchema({schema: basketSchema, res})

    const basket = await basketApi.basket();
    expect(basket.status()).toBe(200);
    expect(await basket.json).toBeDefined();
  });
});

test.describe("Тестовое задание. (Сценарии 1,3,4).", () => {
  test.beforeEach(async ({basketApi}) => {
    await basketApi.clear();

    const res = await (await basketApi.getBasket()).json();
    expect(res.basketCount).toEqual(0)

  });

  test(`Сценарий 1`, async ({basketApi}) => {
    await basketApi.create(1,9);
    const response = await (await basketApi.getBasket()).json();
    expect(response.basketCount).toEqual(9)

    const res: Basket = await (await basketApi.getBasket()).json();
    expect(res.basketCount).toEqual(9)
    expect(res.basket.length).toEqual(9)

    await validateSchema({schema: basketSchema, res})

    const basket = await basketApi.basket();
    expect(basket.status()).toBe(200);
    expect(await basket.json).toBeDefined();
  });

  test(`Сценарий 3`, async ({basketApi}) => {
    await basketApi.create(1,1);
    const response = await (await basketApi.getBasket()).json();
    expect(response.basketCount).toEqual(1)

    const res: Basket = await (await basketApi.getBasket()).json();
    expect(res.basketCount).toEqual(1)
    expect(res.basket.length).toEqual(1)

    await validateSchema({schema: basketSchema, res})

    const basket = await basketApi.basket();
    expect(basket.status()).toBe(200);
    expect(await basket.json).toBeDefined();
  });

  test(`Сценарий 4`, async ({basketApi}) => {
    await basketApi.create(2,1);
    const response = await (await basketApi.getBasket()).json();
    expect(response.basketCount).toEqual(1)

    const res: Basket = await (await basketApi.getBasket()).json();
    expect(res.basketCount).toEqual(1)
    expect(res.basket.length).toEqual(1)

    await validateSchema({schema: basketSchema, res})

    const basket = await basketApi.basket();
    expect(basket.status()).toBe(200);
    expect(await basket.json).toBeDefined();
  });
});
