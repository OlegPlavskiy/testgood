import { searchTest as test } from '../tests';

test.describe("Тестовое задание. (Сценарии 1,3,4).", () => {
  test.beforeEach(async ({ mainPage}) => {
    await mainPage.post('/basket/clear')
    await mainPage.visit('');
    await mainPage.header.checkCountInBasket(0);
  });

  test(`Сценарий 1`, async ({mainPage}) => {
    const countProduct = 9;

    await mainPage.addProduct(true, countProduct);
    await mainPage.header.checkCountInBasket(countProduct);

    await mainPage.header.clickDropdownBasket();
    await mainPage.header.checkDropdownBasket();

    await mainPage.header.clickGoToBasketButton()
    await mainPage.checkCurrentUrl('/basket')
  });

  test(`Сценарий 3`, async ({mainPage}) => {
    await mainPage.clickProduct(true);
    await mainPage.header.checkCountInBasket(1);

    await mainPage.header.clickDropdownBasket();
    await mainPage.header.checkDropdownBasket();

    await mainPage.header.clickGoToBasketButton()
    await mainPage.checkCurrentUrl('/basket')
  });

  test(`Сценарий 4`, async ({mainPage}) => {
    await mainPage.clickProduct(false);
    await mainPage.header.checkCountInBasket(1);

    await mainPage.header.clickDropdownBasket();
    await mainPage.header.checkDropdownBasket();

    await mainPage.header.clickGoToBasketButton()
    await mainPage.checkCurrentUrl('/basket')
  });
});

test.describe("Тестовое задание. (Сценарии 2).", () => {
  test.beforeEach(async ({ mainPage}) => {
    await mainPage.post('/basket/clear', {})
    await mainPage.post('/basket/create', {form: {'product': 1, 'count': 1}})
    await mainPage.visit('');
    await mainPage.header.checkCountInBasket(1);
  });

  test(`Сценарий 2`, async ({mainPage}) => {
    const countProduct = 8;
    for (let i = 0; i < countProduct; i++) {
      const isDiscount = Math.random() < 0.5;
      await mainPage.clickProduct(isDiscount);
    }
    await mainPage.header.checkCountInBasket(9);

    await mainPage.header.clickDropdownBasket();
    await mainPage.header.checkDropdownBasket();

    await mainPage.header.clickGoToBasketButton()
    await mainPage.checkCurrentUrl('/basket')
  });
});
