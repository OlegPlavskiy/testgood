import { APIRequestContext } from "playwright-core";
import * as fs from "fs";
import {BaseApi} from "./baseApi";
const csrf_token = fs.readFileSync('../playwright/.auth/csrf_token.json', 'utf-8')

export class ProductApi extends BaseApi{
    constructor(public request: APIRequestContext) {
        super(request)
    }
    async getProducts(){
        return await this.getReq('/product/get');
    }
}