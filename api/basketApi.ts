import { APIRequestContext } from "playwright-core";
import * as fs from "fs";
import {BaseApi} from "./baseApi";
const csrf_token = fs.readFileSync('../playwright/.auth/csrf_token.json', 'utf-8')

export class BasketApi extends BaseApi{
    constructor(public request: APIRequestContext) {
        super(request)
    }

    async clear(){
        return await this.postReq('/basket/clear', {});
    }
    async create(productId, count){
        return await this.postReq('/basket/create', {form: {'product': productId, 'count': count}});
    }
    async getBasket(){
        return await this.getReq('/basket/get');
    }
    async basket(){
        return await this.getReq('/basket');
    }
}