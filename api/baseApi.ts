import { APIRequestContext } from "playwright-core";
import * as fs from "fs";
const csrf_token = fs.readFileSync('../playwright/.auth/csrf_token.json', 'utf-8')

export class BaseApi {
    constructor(public request: APIRequestContext) {
    }

    async postReq(endpoint: string, postData: object) {
        const res = await this.request.post(endpoint,{...postData,
            headers: {"x-csrf-token": csrf_token as string}
        })
        return res
    }

    async getReq(endpoint: string) {
        const res = await this.request.get(endpoint)
        return res
    }

    async putReq(endpoint: string, reqBody: object, token: string) {
        const res = await this.request.put(endpoint, {
            data: reqBody, headers: {
                'Cookie': `token=${token}`
            }
        })
        return res
    }

    async patchReq(endpoint: string, reqBody: object, token: string) {
        const res = await this.request.patch(endpoint, {
            data: reqBody, headers: {
                'Cookie': `token=${token}`
            }
        })
        return res
    }

    async deleteReq(endpoint: string, token: string) {
        const res = await this.request.delete(endpoint, {
            headers: {
                'Cookie': `token=${token}`
            }
        })
        return res
    }
}