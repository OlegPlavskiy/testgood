import {expect, Page} from "@playwright/test";

import {Button} from "../../page-factory/button";

export class Header {
    readonly page: Page;
    private readonly basketCount: Button;
    private readonly dropdownBasket: Button;
    private readonly basket: Button;
    private readonly basketItemTitle: Button;
    private readonly basketItemPrice: Button;
    private readonly basketPrice: Button;
    private readonly goToBasketButton: Button;


    constructor(page: Page) {
        this.page = page;
        this.basketCount = new Button({ page, locator: "[class='basket-count-items badge badge-primary']", name: 'Колличество товаров в корзине'});
        this.basket = new Button({ page, locator: "[id='dropdownBasket']", name: 'Корзина'});
        this.dropdownBasket = new Button({ page, locator: "[class='dropdown-menu dropdown-menu-right show']", name: 'Дропдаун корзины'});
        this.basketItemTitle = new Button({ page, locator: "[class='basket-item-title']", name: 'Наименование товара'});
        this.basketItemPrice = new Button({ page, locator: "[class='basket-item-price']", name: 'Цена товара'});
        this.basketPrice = new Button({ page, locator: "[class='basket_price']", name: 'Общая цена товара'});
        this.goToBasketButton = new Button({ page, locator: "[class='btn btn-primary btn-sm ml-auto']", name: 'Перейти в корзину'});
    }

    async clickDropdownBasket() {
        await this.basket.click();
    }
    async clickGoToBasketButton() {
        await this.goToBasketButton.click();
    }
    async checkCountInBasket(count: number) {
        await this.basketCount.shouldHaveText(count.toString());
    }
    async checkDropdownBasket(){
        await this.dropdownBasket.shouldBeVisible()
        await this.basketItemTitle.shouldBeVisible()
        await this.basketItemPrice.shouldBeVisible()
        await this.basketPrice.shouldBeVisible()
    }
}
