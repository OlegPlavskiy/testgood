import { BasePage } from './base-page';
import {expect, Locator, Page} from "@playwright/test";
import {Button} from "../page-factory/button";
import {Header} from "../components/modals/header";

export class MainPage extends BasePage {
    readonly header: Header;

    private readonly productsHasDiscount: Button;
    private readonly productsHasNotDiscount: Button;
    private readonly productAddButton: string;
    private readonly productInputCount: string;

    constructor(public page: Page) {
        super(page);
        this.header = new Header(page);

        this.productsHasDiscount = new Button({ page, locator: "[class='note-item card h-100 hasDiscount']", name: 'Товары со скидкой' });
        this.productsHasNotDiscount = new Button({ page, locator: "[class='note-item card h-100']", name: 'Товары без скидки' });
        this.productAddButton = "[class*='actionBuyProduct btn btn-primary mt-3']";
        this.productInputCount = "[name='product-enter-count']";
 }

    async clickProduct(hasDiscount = false, clickCount= 1) {
        const product = await this.getProduct(hasDiscount)
        await product.locator(this.productAddButton).click({clickCount: clickCount})
    }
    async addProduct(hasDiscount = false, count= 1) {
        const product = await this.getProduct(hasDiscount);
        await this.setCountProduct(count, product);
        await product.locator(this.productAddButton).click();
    }
    async setCountProduct(count= 1, product: Locator) {
        await product.locator(this.productInputCount).fill(count.toString());
    }
     async getProduct(hasDiscount: boolean): Promise<Locator> {
        const productLocator = hasDiscount ? await this.productsHasDiscount.getLocator() : await this.productsHasNotDiscount.getLocator();
        const count = await productLocator.count();

        const randomIndex = Math.floor(Math.random() * count);
        return productLocator.nth(randomIndex);
    }
}