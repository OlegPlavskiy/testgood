import test, {expect, Page} from '@playwright/test';
import * as fs from "fs";
const csrf_token = fs.readFileSync('../playwright/.auth/csrf_token.json', 'utf-8')

export class BasePage {

    constructor(public page: Page) {
    }

    async context(){
        return this.page.context();
    }

    async post(url, postData = undefined){
        await this.page.request.post(url, {...postData,
            headers: {"x-csrf-token": csrf_token as string}
        });
    }

    async waitForResponses(urlPart: any) {
        let resolveResponse: any;
        const responsePromise: Promise<Buffer> = new Promise(resolve => resolveResponse = resolve);

        this.page.on('response', async response => {
            if (response.url().includes(urlPart)) {
                const body: Buffer = await response.body();
                resolveResponse(body);
            }
        });

        return responsePromise;
    }

    onResponse(urlPart: string, callback: any) {
        this.page.on('response', async (response) => {
            if (response.url().includes(urlPart)) {
                const jsonResponse = await response.body();
                callback(jsonResponse);
            }
        });
    }

    async visit(url: string): Promise<void> {
        await test.step(`Opening the url "${url}"`, async () => {
            await this.page.goto(url, { waitUntil: 'load' });//'commit' });
        });
    }

    async clearCookies(): Promise<void> {
        const browserContext = await this.context();
        await browserContext.clearCookies();
    }

    async reload(): Promise<void> {
        const currentUrl = this.page.url();
        await test.step(`Reloading page with url "${currentUrl}"`, async () => {
            await this.page.reload({ waitUntil: 'domcontentloaded' });
        });
    }
    async waitForResponse(url: any): Promise<void> {
        await test.step(`Get response`, async () => {
            return await this.page.waitForResponse(url);
        });
    }

    async getUrl(): Promise<string> {
        let currentUrl: string = ""
        await test.step(`Getting current url`, async () => {
            currentUrl = this.page.url();
        });
        return currentUrl;
    }

    async checkCurrentUrl(expectedUrl: string): Promise<void> {
        const currentUrl = await this.getUrl();
        await test.step(`Current url="${currentUrl}" contains "${expectedUrl}"`, async () => {
            expect(await this.getUrl()).toContain(expectedUrl)
        });
    }

    async wait(time: number): Promise<void> {
        await this.page.waitForTimeout(time)
    }
}