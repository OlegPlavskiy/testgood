import { Fixtures, Page, PlaywrightTestArgs } from '@playwright/test';
import { mockStaticRecourses } from '../utils/mocks/static-mock';
import {BasketApi} from "../api/basketApi";
import {ProductApi} from "../api/productApi";

export type ContextPagesFixture = {
    contextPage: Page;
    basketApi: BasketApi;
    productApi: ProductApi;
};

export const contextPagesFixture: Fixtures<ContextPagesFixture, PlaywrightTestArgs> = {
    contextPage: async ({ page }, use) => {
        await mockStaticRecourses(page);
        await use(page);
    },
    basketApi: async ({ request }, use) => {
        const basketApi = new BasketApi(request)
        await use(basketApi);
    },
    productApi: async ({ request }, use) => {
        const productApi = new ProductApi(request)
        await use(productApi);
    }
};