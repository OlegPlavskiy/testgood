import { Fixtures } from '@playwright/test';
import { MainPage } from '../pages/main-page';
import { ContextPagesFixture } from './context-pages';

export type PlaywrightPagesFixture = {
    mainPage: MainPage;
};

export const playwrightPagesFixture: Fixtures<PlaywrightPagesFixture, ContextPagesFixture> = {
    mainPage: async ({ contextPage }, use) => {
        const mainPage = new MainPage(contextPage);

        await use(mainPage);
    }
};